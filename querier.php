<?php

spl_autoload_register(function ($class_name) {
  require('app/lib/' . $class_name . '.php');
});

$config = new Config(Site::ConfigFile);
$db = new DB($config->database);
$db->connect() || die();

if($config->main['Mode'] == 'user') {
  session_start();
  if(isset($_SESSION['username'])) {
    $db->addFilter(DB::USER_LOGIN, $_SESSION['username']);
  } else {
    exit();
  }
}

$sortOrder = array(DB::JOB_DATE, true);
$displayPage = 1;

if(isset($_POST['quota'])) {
  print(json_encode($db->getQuota($_SESSION['username'] ?? 'nobody')));
}elseif(isset($_POST['id'])) {
  // ID foi fornecido: retorna os detalhes do trabalho
  print(json_encode($db->getJobDetail($_POST['id'])));
} else {
  // Lista os trabalhos
  foreach($_POST as $key => $value) {
    if((! $value) || preg_match('/[\(\)]/', $value)) {
      // Valores contendo "(" e ")" (exemplo: "(Todos)") sao ignorados
      continue;
    }
    switch(true) {
      case($key == DB::USER_LOGIN):
        if($config->main['Mode'] == 'admin') {
          $db->addFilter(DB::USER_LOGIN, $value, false);
        }
        break;
      case(($key == 'date_begin') || ($key == 'date_end')):
        if($date = DateTime::createFromFormat('d/m/Y', $value)) {
          $dateParams[$key] = $date->format('Y-m-d ') . ($key == 'date_begin' ? '00:00:00' : '23:59:59');
        }
        break;
      case($key == 'sort_order'):
        $sortOrder = explode(':', $value);
        break;
      case($key == 'display_page'):
        $displayPage = $value;
        break;
      default:
        $db->addFilter($key, $value);
    }
  }

  if(isset($dateParams['date_begin']) && isset($dateParams['date_end'])) {
    $db->addFilter(DB::JOB_DATE, array($dateParams['date_begin'], $dateParams['date_end']));
  }

  print(json_encode($db->getReport($sortOrder[0], $sortOrder[1], $displayPage, $config->report['EntriesPerPage'])));
}
$db->close();

?>
