<?php

spl_autoload_register(function ($class_name) {
  require('app/lib/' . $class_name . '.php');
});

$config = new Config(Site::ConfigFile);
$errMsg = null;

switch($config->main['Mode']) {
  case 'admin':
    $pageMode = array('admin', 'jobview');
    break;
  case 'user':
    session_start();
    if(isset($_GET['logout'])) {
      session_unset();
      session_commit();
      header('Location: ' . $config->main['BaseURL']);
      exit();
    } else {
      $username = $_POST['username'] ?? null;
      $password = $_POST['password'] ?? null;
      if($username && $password) {
        if(pam_auth($username, $password)) {
          $_SESSION['displayName'] = explode(',', posix_getpwnam($username)['gecos'])[0];
          $_SESSION['username'] = $username;
        } else {
          $errMsg = "Nome de usu&aacute;rio ou senha incorreta";
        }
      }
      if(isset($_SESSION['username'])) {
        $pageMode = array('user', 'jobview');
      } else {
        $pageMode = array('login', 'login');
      }
      break;
    }
}

require('app/canvas.php');

?>
