// Numero maximo de paginas exibidas no pager
var reportMaxPages = 20

$(document).ready(function(){
  var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())

  // Gijgo DatePicker com datas de inicio e fim
  $('#input-date_begin').datepicker({
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fontawesome',
    locale: 'pt-br',
    format: 'dd/mm/yyyy',
    maxDate: function () {
      return $('#endDate').val()
    }
  })
  $('#input-date_end').datepicker({
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fontawesome',
    locale: 'pt-br',
    format: 'dd/mm/yyyy',
    minDate: function () {$('#table-report th:first-child span').attr('class', 'fa fa-sort-up')
      return $('#startDate').val()
    }
  })

  // Reinicia os parametros de ordenacao quando o botao de busca for clicado
  $('#btn-update-report').click(function() {
    $('#var-display_page').val(1)
    $('#var-sort_order').val('')
    $('#table-report span').attr('class', 'fa fa-sort')
    $('#table-report th:first-child span').attr('class', 'fa fa-sort-up')
  })

  // Altera a ordenacao dos resultados
  $('#table-report th').click(function() {
    var currentSortOrder = $('#var-sort_order').val().split(':')
    $('#table-report th span').attr('class', 'fa fa-sort')
    if($(this).data("sort_by") == currentSortOrder[0]) {
      // Usuario selecionou o criterio atual: apenas inverte a ordem
      if(currentSortOrder[1] == '0') {
        $('#var-sort_order').val(currentSortOrder[0] + ':1')
        $(this).children('span').attr('class', 'fa fa-sort-up')
    } else {
      $('#var-sort_order').val(currentSortOrder[0] + ':0')
      $(this).children('span').attr('class', 'fa fa-sort-down')
    }
    } else {
      // Usuario selecionou outro criterio de selecao
      $('#var-sort_order').val($(this).data("sort_by") + ':0')
      $(this).children('span').attr('class', 'fa fa-sort-down')
    }
    $('#var-display_page').val(1)
    $('#form-report-query').submit()
  })

  // Exibe os detalhes do trabalho
  $(document).on('click', '#table-report-data tr', function() {
    showDetails($(this).data('id'))
  })

  // Exibe os detalhes da quota de impressao
  $(document).on('click', '#href-show-quota', function() {
    showQuota()
  })

  // Navega pelo pager
  $(document).on('click', '.page-link', function() {
    $('#var-display_page').val($(this).data('page'))
    $('#form-report-query').submit()
  })

  // Envia os dados do formulario e recebe o resultado da pesquisa
  $('#form-report-query').submit(function(event) {
    event.preventDefault()
    loadReport()
  })

  // Carrega o relatorio inicial
  loadReport()

})



/*
* Submete os dados do formulario e carrega o relatorio
*/
function loadReport() {
  // Esvazia a tabela de resultados
  $('#table-report-data').empty()
  // Submete o formulario e obtem a resposta (formato json)
  $.post('querier.php', $('#form-report-query').serialize(), function(query) {
    if(query.jobcount > 0) {
      // Encontrou pelo menos 1 registro: popula a tabela
      $('#message').html(query.pages > reportMaxPages ? `Aten&ccedil;&atilde;o: sua busca retornou muitos resultados.  Apenas as primeiras ${reportMaxPages} p&aacute;ginas ser&atilde;o exibidas.<br>` : '')
      $('#message').append(`Encontrada${_s(query.sheetcount)} ${query.sheetcount} folha${_s(query.sheetcount)} impressa${_s(query.sheetcount)} em ${query.jobcount} trabalho${_s(query.jobcount)}.`)
      $('#table-report').show()
      // Corpo da tabela
      $.each(query.data, (function(i, value) {
        // Abrevia nomes de mais longos do que 80 caracteres
        if(value.jobtitle.length > 80) {
          value.jobtitle = value.jobtitle.substring(0, 80) + '...'
        }
        // Quota liberada consta no banco de dados como userprintquota = -65535
        if(value.userprintquota == -65535) {
          value.userprintquota = '<b class="infinity">&#x221e;</b>'
        }
        // Polls de impressao
        if(value.printerpollmember != 0) {
          value.printername += '[' + value.printerpollmember + ']'
        }
        // Monta a tabela do corpo do relatorio
        var jobresult = {
          class : 'fa-bomb job-result-fail',
          tip   : 'Trabalho N&Atilde;O impresso'
        }
        switch(value.jobresult) {
          case 'ALLOW':
            jobresult = {
              class : 'fa-check job-result-ok',
              tip   : 'Trabalho impresso'
            }
            break
          case 'DENY':
            jobresult['class'] = 'fa-times-circle job-result-fail'
        }
        $('#table-report-data').append(`
          <tr data-id="${value.id}">
            <td>${value.strjobdate}</td>
            ${ $('main').data('mode') == 'admin' ? `<td>${value.userlogin}</td><td>${value.usergroup}</td>` : '' }
            <td>${value.jobtitle}</td>
            <td align="center">${value.printername}</td>
            <td align="center">${value.jobsizepages}</td>
            <td align="center"><span data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="left" title="Quota utilizada:<br />${value.userpagecounter} ${isNaN(value.userprintquota) ? '' : ' de ' + value.userprintquota} folhas">${value.userpagecounter} / ${value.userprintquota}</span></td>
            <td align="center"><i class="fa ${jobresult['class']}" title="${jobresult['tip']}" data-bs-toggle="tooltip" data-bs-placement="left"></i></td>
          </tr>
        `)
      }))
      // Ativa tooltips dos elementos da tabela
      $('[data-bs-toggle="tooltip"]').each(function() {
        new bootstrap.Tooltip($(this).get(0))
      })
      // Insere o pager caso a saida tenha mais do que 1 pagina
      if(query.pages > 1) {
        var lastPage = Math.min(query.pages, reportMaxPages)
        $('#pager-container').show()
        $('#pager-contents').empty()
        for(var i = 1; i <= lastPage; i++) {
          active = $('#var-display_page').val() == i ? 'active' : ''
          $('#pager-contents').append(`<li class="page-item ${active}"><a class="page-link" data-page="${i}" href="javascript:void(0)">${i}</a></li>`)
        }
      } else {
        // Conteudo cabe em 1 pagina: esconde o pager
        $('#pager-container').hide()
      }
    } else {
      // Nenhum trabalho encontrado
      $('#table-report').hide()
      $('#pager-container').hide()
      $('#message').html('Nenhum trabalho encontrado.')
    }
  }, 'json')
}


/* 
 * Popula o "modal" de detalhes do trabalho
 */
function showDetails(id) {
  $.post('querier.php', { id: id }, function(data) {
    $('#job-detail_title').html(data.jobtitle)
    $('#job-detail_date').html(data.strjobdate)
    $('#job-detail_id').html(data.jobid)
    $('#job-detail_size').html(`${data.jobsizepages} folha${_s(data.jobsizepages)}` + 
      ( $('main').data('mode') == 'admin' ? ` (pr&eacute;-estimado: ${data.precomputedjobsizepages} folha${_s(data.precomputedjobsizepages)})` : '' ) +
      ` / ${data.jobsizebytes.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')} bytes`)
    $('#job-detail_login').html(data.userlogin)
    $('#job-detail_origin').html(data.originatinghostname + (data.originatinghostname != data.originatinghostaddr ? ' (' + data.originatinghostaddr + ')' : ''))
    $('#modal-job-detail').modal('show')
  }, 'json')
}


function showQuota() {
  $.post('querier.php', { quota: true }, function(data) {
    $('#modal-print-quota tbody').empty()
    $.each(data, function(i, item){
      $('#modal-print-quota tbody').append(`<tr><td>${item.printer}</td><td>${item.limit ?? '(ilimitada)'}</td><td>${item.counter}</td><td>${item.limit ? item.limit - item.counter : '(ilimitada)'}</td></tr>`)
    })
    $('#modal-print-quota').modal('show')
  }, 'json')
}


/*
 * Retorna um 's' para formar o plural das palavras
 */
function _s(val) {
  return val == 1 ? '' : 's'
}
