<?php

class Config {

  public $database = [];
  public $report = [];
  public $main = [];

  function __construct($configFile) {
    $config = [];
    if($config = parse_ini_file($configFile, 1)) {
      if(! in_array($config['main']['Mode'], array('admin', 'user'))) {
        die('Modo de operacao invalido: ' .  $config['main']['Mode']);
      }
      $this->database['accounting'] = $config['accounting_database'];
      $this->database['quota'] = $config['quota_database'];
      $this->database['quota']['ValidPrinters'] = explode(' ', $this->database['quota']['ValidPrinters']);
      $this->report = $config['report'];
      $this->main = $config['main'];
      $this->main['QuotaResetDate'] = $this->closestFutureDate(explode(' ', $this->main['QuotaResetDates']));
    } else {
      die("Erro abrindo arquivo de configuracao: $configFile");
    }
  }

  private function closestFutureDate($dates) {
    $now = time();
    $closestDate = PHP_INT_MAX;
    foreach($dates as $date) {
      $date = strtotime($date) > $now ? strtotime($date) : strtotime("$date + 1 year");
      if(($date > $now) && ($date < $closestDate)) {
        $closestDate = $date;
      }
    }
    return($closestDate == PHP_INT_MAX ? null : date('d/m/Y', $closestDate));
  }

}

?>
