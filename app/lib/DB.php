<?php

class DB {

  const JOB_DATE     = 'jobdate';
  const JOB_TITLE    = 'jobtitle';
  const JOB_SHEETS   = 'jobsizepages';
  const JOB_RESULT   = 'jobresult';
  const USER_LOGIN   = 'userlogin';
  const USER_GROUP   = 'usergroup';
  const USER_PAGECOUNTER  = 'userpagecounter';
  const PRINTER_NAME = 'printername';

  private $accountingDbConn = '';
  private $quotaDbConn = '';
  private $quotaPrinters = array();
  private $queryFilter = [];

  function __construct($config) {
    $this->accountingDbConn = sprintf("host=%s dbname=%s user=%s password=%s",
      $config['accounting']['Host'], $config['accounting']['Name'], $config['accounting']['User'], $config['accounting']['Password']);
    $this->quotaDbConn = sprintf("host=%s dbname=%s user=%s password=%s",
      $config['quota']['Host'], $config['quota']['Name'], $config['quota']['User'], $config['quota']['Password']);
    $this->quotaPrinters = $config['quota']['ValidPrinters'];
  }

  function connect($database = 'accounting') {
    $dbConn = $database . 'DbConn';
    return ($this->$dbConn = pg_connect($this->$dbConn)) ? true : false;
  }

  function close($database = 'accounting') {
    $dbConn = $database . 'DbConn';
    pg_close($this->$dbConn);
  }

  function listValues($column) {
    if($result = pg_query($this->accountingDbConn, "SELECT DISTINCT $column FROM jobhistory WHERE $column <> '(unknown)' ORDER BY $column")) {
      return pg_fetch_all_columns($result);
    }
    return false;
  }

  function addFilter($key, $value, $equal = true) {
    if(is_array($value)) {
      $this->queryFilter[] = "$key BETWEEN '${value[0]}' AND '${value[1]}'";
    } else {
      $this->queryFilter[] = $equal ? "$key = '$value'" : "$key like('%$value%')";
    }
  }

  function getJobDetail($id) {
    $queryString = "SELECT jobid, jobtitle, to_char(jobdate, 'DD/MM/YYYY - HH24:MI:SS') AS strjobdate, jobsizepages, precomputedjobsizepages, jobsizebytes, userlogin, originatinghostname, originatinghostaddr FROM jobhistory WHERE id = $id";
    if($result = pg_query($this->accountingDbConn, $queryString)) {
      return pg_fetch_array($result, 0, PGSQL_ASSOC);
    }
    return array();
  }

  function getReport($order = '', $reverseOrder = false, $page = 1, $pageSize = 25) {
    $report['jobcount']   = 0;
    $report['sheetcount'] = 0;
    $queryFilter = $this->queryFilter ? ' WHERE ' . implode(' AND ', $this->queryFilter) : '';
    $queryParameters = $order ? " ORDER BY $order " . ($reverseOrder ? 'DESC' : 'ASC') : '';
    $queryParameters .= ' OFFSET ' . ($page - 1) * $pageSize .' LIMIT ' . $pageSize;

    $queryString = 'SELECT COUNT(id) AS jobcount, SUM(jobsizepages) AS pagecount FROM jobhistory ' . $queryFilter;
    if($result = pg_query($this->accountingDbConn, $queryString)) {
      $counters = pg_fetch_assoc($result);
      $report['jobcount']   = number_format(intval($counters['jobcount']), 0, ',', '.');
      $report['sheetcount'] = number_format(intval($counters['pagecount']), 0, ',', '.');
      $report['pages'] = ceil($counters['jobcount'] / $pageSize);
      $queryString = "SELECT id, to_char(jobdate, 'DD/MM/YYYY HH24:MI:SS') AS strjobdate, jobdate, userlogin, usergroup, jobtitle, printername, printerpollmember, jobsizepages, userpagecounter, userprintquota, jobresult FROM jobhistory $queryFilter $queryParameters";
      if($result = pg_query($this->accountingDbConn, $queryString)) {
        $report['data'] = pg_fetch_all($result);
      }
    }

    return $report;
  }

  function getQuota($username) {
    $quota = array();
    if($this->connect('quota')) {
      if($result = pg_query($this->quotaDbConn, 'SELECT printername, pagecounter, hardlimit FROM userpquota ' .
        'INNER JOIN users ON userpquota.userid = users.id INNER JOIN printers ON userpquota.printerid = printers.id ' .
        "WHERE username = '$username' ORDER BY hardlimit DESC"))
        foreach(pg_fetch_all($result) as $quotaItem) {
          if(in_array($quotaItem['printername'], $this->quotaPrinters)) {
            $quota[] = array(
              'printer' => $quotaItem['printername'],
              'counter' => $quotaItem['pagecounter'],
              'limit'   => $quotaItem['hardlimit']
            );
          }
        }
      $this->close('quota');
    }
    return $quota;
  }

}

?>
