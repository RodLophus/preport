<div class="container-fluid">
  <p id="message"></p>
  <div id="pager-container" style="display:none">
    <ul class="pagination justify-content-center" id="pager-contents"></ul>
  </div>
  <table class="table table-striped table-hover" id="table-report" style="display:none">
    <thead>
      <tr>
        <th scope="col" width="190px" data-sort_by="<?=DB::JOB_DATE?>"><span class="fa fa-sort-up" aria-hidden="true"></span> Data / Hora</th>
        <th scope="col" width="120px" data-sort_by="<?=DB::USER_LOGIN?>"><span class="fa fa-sort" aria-hidden="true"></span> Login</th>
        <th scope="col" width="120px" data-sort_by="<?=DB::USER_GROUP?>"><span class="fa fa-sort" aria-hidden="true"></span> Grupo</th>
        <th scope="col" data-sort_by="<?=DB::JOB_TITLE?>"><span class="fa fa-sort" aria-hidden="true"></span> Nome do trabalho</th>
        <th scope="col" width="140px" data-sort_by="<?=DB::PRINTER_NAME?>"><span class="fa fa-sort" aria-hidden="true"></span> Impressora</th>
        <th scope="col" width="100px" data-sort_by="<?=DB::JOB_SHEETS?>"><span class="fa fa-sort" aria-hidden="true"></span> Folhas</th>
        <th scope="col" width="120px" data-sort_by="<?=DB::USER_PAGECOUNTER?>"><span class="fa fa-sort" aria-hidden="true"></span> Quota </th>
        <th scope="col" width="140px" data-sort_by="<?=DB::JOB_RESULT?>"><span class="fa fa-sort" aria-hidden="true"></span> Resultado</th>
      </tr>
    </thead>
    <tbody id="table-report-data">
    </tbody>
  </table>
</div>
