<?php
  $db = new DB($config->database);
  $db->connect();
  $groups   = $db->listValues(DB::USER_GROUP);
  $printers = $db->listValues(DB::PRINTER_NAME);
  $results  = $db->listValues(DB::JOB_RESULT);
  $db->close();
?>
<div class="container-fluid">
  <div class="row w-100">
    <div class="col-3"></div>
    <div class="col-6">
      <form id="form-report-query">
        <div class="row py-1 gx-1">
          <div class="col-4 form-group">
            <label for="input-date_begin input-date_end">Data:</label>
            <input type="text" class="form-control" id="input-date_begin" name="date_begin" placeholder="De (DD/MM/AAAA)">
          </div>
          <div class="col-2 form-group">
            <label for="input-login">Login:</label>
            <input type="text" class="form-control" id="input-login" name="<?=DB::USER_LOGIN?>" placeholder="(Todos)">
          </div>
          <div class="col-2 form-group">
            <label for="select-group">Grupo:</label>
            <select class="form-control custom-select" id="select-group" name="<?=DB::USER_GROUP?>">
              <option>(Todos)</option>
              <?php foreach($groups as $group) { ?>
              <option><?=$group?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-2 form-group">
            <label for="select-printer">Impressora:</label>
            <select class="form-control custom-select" id="select-printer" name="<?=DB::PRINTER_NAME?>">
              <option>(Todas)</option>
              <?php foreach($printers as $printer) { ?>
              <option><?=$printer?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-2 form-group">
            <label for="select-result">Resultado:</label>
            <select class="form-control custom-select" id="select-result" name="<?=DB::JOB_RESULT?>">
              <option>(Todos)</option>
              <?php foreach($results as $result) { ?>
              <option><?=$result?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="row py-1 gx-1">
          <div class="col-4">
            <input type="text" class="form-control" id="input-date_end" name="date_end" placeholder="At&eacute; (DD/MM/AAAA)">
          </div>
          <div class="col-8">
            <input type="submit" class="btn btn-primary" id="btn-update-report" value="Gerar Relat&oacute;rio"/>
          </div>
        </div>
        <input type="hidden" id="var-sort_order" name="sort_order" value="" />
        <input type="hidden" id="var-display_page" name="display_page" value="1" />
      </form>
    </div>
    <div class="col-3"></div>
  </div>
</div>