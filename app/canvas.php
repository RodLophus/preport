<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="lib/modules/bootstrap/dist/css/bootstrap.min.css" />
  <link rel="stylesheet" href="lib/modules/fontawesome-free/css/all.min.css" />
  <link rel="stylesheet" href="lib/modules/gijgo/css/gijgo.min.css" />
  <link rel="stylesheet" href="lib/site/css/site.css" />
  <script src="lib/modules/jquery/dist/jquery.min.js"></script>
  <script src="lib/modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="lib/modules/gijgo/js/gijgo.min.js"></script>
  <script src="lib/modules/gijgo/js/messages/messages.pt-br.min.js" type="text/javascript"></script>
  <script src="lib/site/js/<?=$pageMode[1]?>.js"></script>
  <title><?=Site::Title?> (v.<?=Site::Version?>)</title>
</head>
<body>
  <nav class="navbar navbar-expand navbar-dark bg-dark d-flex justify-content-center">
    <?php require('app/navbar_' . $pageMode[0] . '.inc.php'); ?>
  </nav>
  <main data-mode="<?=$pageMode[0]?>">
    <?php require('app/main_' . $pageMode[0] . '.inc.php'); ?>
  </main>
  <!-- Detalhes do trabalho (modal) -->
  <div class="modal fade" id="modal-job-detail" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-job-detail-title">Detalhes do Trabalho</h5>
          <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal"></button>
        </div>
        <div class="modal-body">
          <table class="table table-sm">
            <tbody>
              <tr>
                <th scope="row">T&iacute;tulo:</th>
                <td id="job-detail_title"></td>
              </tr>
              <tr>
                <th scope="row">Data:</th>
                <td id="job-detail_date"></td>
              </tr>
              <tr>
                <th scope="row">ID:</th>
                <td id="job-detail_id"></td>
              </tr>
              <tr>
                <th scope="row">Tamanho:</th>
                <td id="job-detail_size"></td>
              </tr>
              <tr>
                <th scope="row">Usu&aacute;rio:</th>
                <td id="job-detail_login"></td>
              </tr>
              <tr>
                <th scope="row">Computador:</th>
                <td id="job-detail_origin"></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- Quota de impressao (modal) -->
  <div class="modal fade" id="modal-print-quota" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Quota de Impress&atilde;o Atual</h5>
          <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal"></button>
        </div>
        <div class="modal-body pb-0">
          <table class="table table-sm">
            <thead class="text-center">
              <tr>
                <th>Impressora</th>
                <th>Quota fornecida</th>
                <th>Quota utilizada</th>
                <th>Quota dispon&iacute;vel</th>
              </tr>
            </thead>
            <tbody class="text-center">
            </tbody>
          </table>
          <p><i class="fa fa-hand-point-right pe-2"></i>Sua quota de impress&atilde;o ser&aacute; reinicializada no dia <strong><?=$config->main['QuotaResetDate']?></strong>.</p>

        </div>
        <div class="modal-footer justify-content-start">
        <p><?=$config->main['QuotaInfoFootnote']?></p>
</div>
      </div>
    </div>
  </div>
</body>
</html>
