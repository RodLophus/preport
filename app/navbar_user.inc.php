<div class="container-fluid">
  <div class="row w-100">
    <div class="col-4"></div>
    <div class="col-4">
      <form id="form-report-query">
        <div class="row py-1 gx-1">
          <div class="col form-group">
            <label for="input-date_begin input-date_begin">Data:</label>
            <input type="text" class="form-control" id="input-date_begin" name="date_begin" placeholder="De (DD/MM/AAAA)">
          </div>
          <div class="col">
            <label for="input-date_begin input-date_end">&nbsp;</label>
            <input type="text" class="form-control" id="input-date_end" name="date_end" placeholder="At&eacute; (DD/MM/AAAA)">
          </div>
        </div>
        <div class="row py-1 gx-1">
          <div class="col">
            <input type="submit" class="btn btn-primary" id="btn-update-report" value="Gerar Relat&oacute;rio"/>
          </div>
        </div>
        <input type="hidden" id="var-sort_order" name="sort_order" value="" />
        <input type="hidden" id="var-display_page" name="display_page" value="1" />
      </form>
    </div>
    <div class="col-4 d-flex justify-content-end align-items-center">
      <div class="dropdown">
        <a class="btn btn-dark" href="javascript:void(0)" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          <span class="fas fa-icon fa-3x fa-user" aria-hidden="true"></span>
        </a>
        <ul class="dropdown-menu dropdown-menu-end"">
          <li class="dropdown-item-text text-nowrap">
            <strong><?=$_SESSION['displayName']?></strong><br />
          </li>
          <li><hr class="dropdown-divider"></li>
          <li><a class="dropdown-item" href="javascript:void(0)" id="href-show-quota"><i class="fas fa-copy pe-2"></i>Quota de Impress&atilde;o</a></li>
          <li><a class="dropdown-item" href="?logout"><i class="fas fa-sign-out-alt pe-2"></i>Sair</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>