<main>
  <div class="container-fluid">
    <div class="d-flex justify-content-center">
      <div class="card" id="login-form">
        <div class="card-body">
          <form method="POST">
            <div class="d-flex flex-column justify-content-center">
              <h2 class="mt-3 mx-auto">Login</h2>
              <p class="my-0 py-0 mx-auto text-danger"><?=$errMsg ? "<i class=\"fa fa-times-circle pe-1\"></i>$errMsg" : '&nbsp;'?></p>
            </div>
            <div class="input-group my-3 px-2">
              <span class="fas fa-user input-group-text bg-white border-end-0 pt-2"></span>
              <input type="text" class="form-control border-start-0 ps-0" id="login-form-username" name="username" placeholder="Nome de usu&aacute;rio" value="<?=$_POST['username'] ?? null?>">
            </div>
            <div class="input-group my-3 px-2">
              <span class="fas fa-lock input-group-text bg-white border-end-0 pt-2"></span>
              <input type="password" class="form-control border-start-0 ps-0" id="login-form-password" name="password" placeholder="Senha">
            </div>
            <div class="d-grid my-3 px-2">
              <button class="btn btn-primary" type="submit" disabled>Entrar</button>
            </div>
            <input type="hidden" id="login-form-mode" name="form_mode" value="login">
          </form>
        </div>
      </div>
    </div>
  </div><!--container-->
</main>
